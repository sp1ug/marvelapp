package info.diegocdl.marvelapp;

import android.app.Application;

import java.io.IOException;

import info.diegocdl.marvelapp.data.MarvelApiEndpointInterface;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Diego on 4/25/2016.
 */
public class App extends Application {

    protected Retrofit retrofit;
    public App() {
        super();
        // Define the interceptor, add authentication headers
        Interceptor interceptor = new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                HttpUrl url = request.url()
                        .newBuilder()
                        .addQueryParameter("ts", MarvelApiEndpointInterface.TS )
                        .addQueryParameter("hash", MarvelApiEndpointInterface.HASH)
                        .addQueryParameter("apikey", MarvelApiEndpointInterface.API_KEY)
                        .build();
                request = request.newBuilder().url(url).build();
                return chain.proceed(request);
            }
        };

        // Add the interceptor to OkHttpClient
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(interceptor);
        OkHttpClient client = builder.build();

        retrofit = new Retrofit.Builder()
                .baseUrl(MarvelApiEndpointInterface.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }
}
