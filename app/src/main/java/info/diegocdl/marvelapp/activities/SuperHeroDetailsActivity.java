package info.diegocdl.marvelapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import info.diegocdl.marvelapp.R;
import info.diegocdl.marvelapp.data.super_hero_list.Result;
import info.diegocdl.marvelapp.fragments.SuperHeroContentFragmentListener;

public class SuperHeroDetailsActivity extends AppCompatActivity {

    public static final String OBJECT = "OBJECT";
    protected SuperHeroContentFragmentListener contentFragmentListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_super_hero_details);
        ButterKnife.bind(this);

        contentFragmentListener = (SuperHeroContentFragmentListener)getSupportFragmentManager()
                                                                .findFragmentById(R.id.content);

        Intent i = getIntent();
        Result info = (Result)i.getSerializableExtra(OBJECT);
        contentFragmentListener.setSuperHeroInfo(info);
    }

}
