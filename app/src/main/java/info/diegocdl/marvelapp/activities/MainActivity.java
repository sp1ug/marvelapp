package info.diegocdl.marvelapp.activities;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.EditText;

import butterknife.ButterKnife;
import info.diegocdl.marvelapp.R;
import info.diegocdl.marvelapp.data.PagerAdapter;

public class MainActivity extends AppCompatActivity implements android.support.v7.app.ActionBar.TabListener {
    final static String TAG = "MainActivity";
    private Toolbar mToolbar;
    protected PagerAdapter pagerAdapter;
    private ViewPager mViewPager;
    protected android.support.v7.app.ActionBar.Tab superHerosTab;
    protected android.support.v7.app.ActionBar.Tab mapTab;
    public CoordinatorLayout coordinatorLayout;

    private MenuItem mSearchAction;
    private boolean isSearchOpened = false;
    private EditText edtSeach;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

//        mToolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(mToolbar);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(android.support.v7.app.ActionBar.NAVIGATION_MODE_TABS);
        pagerAdapter =
                new PagerAdapter(
                        getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(pagerAdapter);
        mViewPager.setOnPageChangeListener(
                new ViewPager.SimpleOnPageChangeListener() {
                    @Override
                    public void onPageSelected(int position) {
                        actionBar.setSelectedNavigationItem(position);
                    }
                });

        superHerosTab   = actionBar.newTab().setText("Super Hero List").setTabListener(this);
        mapTab          = actionBar.newTab().setText("Map").setTabListener(this);
        actionBar.addTab(superHerosTab);
        actionBar.addTab(mapTab);


    }

    @Override
    public void onTabSelected(android.support.v7.app.ActionBar.Tab tab, FragmentTransaction ft) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(android.support.v7.app.ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public void onTabReselected(android.support.v7.app.ActionBar.Tab tab, FragmentTransaction ft) {

    }


}
