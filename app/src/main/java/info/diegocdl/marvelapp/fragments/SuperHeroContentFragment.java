package info.diegocdl.marvelapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.Bind;
import butterknife.ButterKnife;
import info.diegocdl.marvelapp.R;
import info.diegocdl.marvelapp.data.super_hero_list.Result;
import info.diegocdl.marvelapp.data.super_hero_list.Thumbnail;

public class SuperHeroContentFragment extends Fragment implements SuperHeroContentFragmentListener {

    @Bind(R.id.superHeroImg)
    ImageView superHeroImg;
    @Bind(R.id.txtSuperHeroName)
    TextView txtSuperHeroName;
    @Bind(R.id.txtDescription)  TextView txtDescription;
    @Bind(R.id.txtNumComics)  TextView txtNumComics;
    @Bind(R.id.txtNumSeries) TextView txtNumSeries;
    @Bind(R.id.txtNumStories) TextView txtNumStories;
    @Bind(R.id.txtNumEvents) TextView txtNumEvents;

    public SuperHeroContentFragment() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_super_hero_content, container, false);

        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void setSuperHeroInfo(Result info) {
        txtSuperHeroName.setText(info.getName());
        txtDescription.setText(info.getDescription());

        String strComics = getString(R.string.main_message_comics_num);
        strComics = String.format(strComics, info.getComics().getAvailable());
        txtNumComics.setText(strComics);

        String strSeries = getString(R.string.main_message_series_num);
        strSeries = String.format(strSeries, info.getSeries().getAvailable());
        txtNumSeries.setText(strSeries);

        String strStories = getString(R.string.main_message_stories_num);
        strStories = String.format(strStories, info.getStories().getAvailable());
        txtNumStories.setText(strStories);

        String strEvents = getString(R.string.main_message_events_num);
        strEvents = String.format(strEvents, info.getEvents().getAvailable());
        txtNumEvents.setText(strEvents);

        Thumbnail thumbnail = info.getThumbnail();
        String iconUrl = thumbnail.getPath() + "." + thumbnail.getExtension();
        Glide.with(this).load(iconUrl).into(superHeroImg);
    }
}
