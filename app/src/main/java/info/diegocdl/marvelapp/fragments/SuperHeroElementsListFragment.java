package info.diegocdl.marvelapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import info.diegocdl.marvelapp.App;
import info.diegocdl.marvelapp.R;
import info.diegocdl.marvelapp.activities.MainActivity;
import info.diegocdl.marvelapp.activities.SuperHeroDetailsActivity;
import info.diegocdl.marvelapp.data.MarvelApiEndpointInterface;
import info.diegocdl.marvelapp.data.OnItemClickListener;
import info.diegocdl.marvelapp.data.super_hero_list.Result;
import info.diegocdl.marvelapp.data.SuperHeroElementsAdapter;
import info.diegocdl.marvelapp.data.super_hero_list.SuperHeroList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class SuperHeroElementsListFragment extends Fragment implements OnItemClickListener {
    protected static final String TAG = "SuperHeroElementList";
    protected Retrofit retrofit;


    protected SuperHeroElementsAdapter adapter;
    @Bind(R.id.searchSuperHeroName) EditText searcEditText;
    @Bind(R.id.elementsList) RecyclerView elementsList;

    public SuperHeroElementsListFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        retrofit = ((App)getActivity().getApplication()).getRetrofit();
    }

    public void initAdapter() {
        adapter = new SuperHeroElementsAdapter(getContext());

        MarvelApiEndpointInterface apiService = retrofit.create(MarvelApiEndpointInterface.class);
        Call<SuperHeroList> call = apiService.getSuperHeroList();
        call.enqueue(new Callback<SuperHeroList>() {
            @Override
            public void onResponse(Call<SuperHeroList> call, Response<SuperHeroList> response) {
                int statusCode = response.code();
                if(statusCode != 200) {
                    CoordinatorLayout coordinatorLayout = ((MainActivity)getActivity()).coordinatorLayout;
                    Snackbar.make(coordinatorLayout, "Error status code: " + statusCode, Snackbar.LENGTH_LONG).show();
                } else {
                    SuperHeroList superHeroList = response.body();
                    List<Result> results = superHeroList.getData().getResults();
                    for(Result r: results){
                        adapter.addElement(r);
                    }
                }

            }

            @Override
            public void onFailure(Call<SuperHeroList> call, Throwable t) {
                CoordinatorLayout coordinatorLayout = ((MainActivity)getActivity()).coordinatorLayout;
                Snackbar.make(coordinatorLayout, "Error al obtener datos de marvel api", Snackbar.LENGTH_LONG).show();
            }
        });
    }

    public void initRecyclerView() {
        elementsList.setLayoutManager(new LinearLayoutManager(getActivity()));
        elementsList.setAdapter(adapter);
        adapter.setOnItemClickListener(this);
    }

    @OnClick(R.id.btnSubmit)
    public void clickHandler() {
        String superHero = searcEditText.getText().toString();
        if(!superHero.trim().equals("")) {
            MarvelApiEndpointInterface apiService = retrofit.create(MarvelApiEndpointInterface.class);
            Call<SuperHeroList> call = apiService.getSuperHeroList(superHero);
            call.enqueue(new Callback<SuperHeroList>() {
                @Override
                public void onResponse(Call<SuperHeroList> call, Response<SuperHeroList> response) {
                    int statusCode = response.code();
                    if(statusCode != 200) {
                        CoordinatorLayout coordinatorLayout = ((MainActivity)getActivity()).coordinatorLayout;
                        Snackbar.make(coordinatorLayout, "Error status code: " + statusCode, Snackbar.LENGTH_LONG).show();
                    }
                    SuperHeroList superHeroList = response.body();
                    if(superHeroList != null){
                        List<Result> results = superHeroList.getData().getResults();
                        adapter.clear();
                        for(Result r: results){
                            adapter.addElement(r);
                        }
                    }
                }

                @Override
                public void onFailure(Call<SuperHeroList> call, Throwable t) {
                    CoordinatorLayout coordinatorLayout = ((MainActivity)getActivity()).coordinatorLayout;
                    Snackbar.make(coordinatorLayout, "Error al obtener datos del api", Snackbar.LENGTH_SHORT).show();
                }
            });
        } else {
            CoordinatorLayout coordinatorLayout = ((MainActivity)getActivity()).coordinatorLayout;
            Snackbar.make(coordinatorLayout, "Debe ingresar un texto para buscar", Snackbar.LENGTH_LONG).show();
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_super_hero_elements_list, container, false);
        ButterKnife.bind(this, v);
        initAdapter();
        initRecyclerView();
        return v;
    }

    @Override
    public void onItemClick(Result info) {
        Intent i = new Intent(getActivity(), SuperHeroDetailsActivity.class);
        i.putExtra(SuperHeroDetailsActivity.OBJECT, info);
        startActivity(i);
    }

}
