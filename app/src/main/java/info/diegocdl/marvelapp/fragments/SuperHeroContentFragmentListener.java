package info.diegocdl.marvelapp.fragments;

import info.diegocdl.marvelapp.data.super_hero_list.Result;

/**
 * Created by Diego on 3/3/2016.
 */
public interface SuperHeroContentFragmentListener {
    void setSuperHeroInfo(Result info);
}
