package info.diegocdl.marvelapp.data;

/**
 * Created by Diego on 3/3/2016.
 */
public class SuperHeroElement {
    protected String title;
    protected String category;
    protected String Uri;

    public SuperHeroElement() {

    }

    public SuperHeroElement(String title, String category) {
        this.title = title;
        this.category = category;
    }
    public String getUri() {
        return Uri;
    }

    public void setUri(String uri) {
        Uri = uri;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
