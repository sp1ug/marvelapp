package info.diegocdl.marvelapp.data;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import info.diegocdl.marvelapp.R;
import info.diegocdl.marvelapp.data.super_hero_list.Result;
import info.diegocdl.marvelapp.data.super_hero_list.Thumbnail;

/**
 * Created by Diego on 3/3/2016.
 */
public class SuperHeroElementsAdapter extends RecyclerView.Adapter<SuperHeroElementsAdapter.ViewHolder> {
    protected ArrayList<Result> data;
    private OnItemClickListener clickListener;
    protected Context context;

    public SuperHeroElementsAdapter(Context context) {
        this.context = context;
        data = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.super_hero_element, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Result element = data.get(position);

        holder.name.setText(element.getName());
        Thumbnail thumbnail = element.getThumbnail();
        String iconUrl = thumbnail.getPath() + "." +  thumbnail.getExtension() ;
        Glide.with(context).load(iconUrl).into(holder.icon);
        if(this.clickListener != null) {
            holder.setOnItemClickListener(element, clickListener);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void addElement(Result e) {
        data.add(e);
        notifyDataSetChanged();
    }

    public void clear() {
        data.clear();
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.clickListener = onItemClickListener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        protected View view;

        @Bind(R.id.name)        TextView name;
        @Bind(R.id.icon)        ImageView icon;

        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            ButterKnife.bind(this, itemView);
        }

        public void setOnItemClickListener(final Result element,
                                           final OnItemClickListener listener) {
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(element);
                }
            });

        }

    }

}
