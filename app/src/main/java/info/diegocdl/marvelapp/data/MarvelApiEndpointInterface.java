package info.diegocdl.marvelapp.data;

import info.diegocdl.marvelapp.data.super_hero_list.SuperHeroList;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Diego on 4/24/2016.
 */
public interface MarvelApiEndpointInterface {
    public static final String BASE_URL = "http://gateway.marvel.com";
    public static final String TS = "1461287446";
    public static final String HASH = "f4e1f478bd13713fee3b8bdb5ba7df05";
    public static final String API_KEY = "49ade587b8e97ca5277b8d2e46350f8a";

    @GET("/v1/public/characters")
    Call<SuperHeroList> getSuperHeroList();

    @GET("/v1/public/characters")
    Call<SuperHeroList> getSuperHeroList(@Query("nameStartsWith") String nameStartsWith);

}
